/*
* This file is part of the hoverboard-firmware-hack project.
*
* Copyright (C) 2017-2018 Rene Hopf <renehopf@mac.com>
* Copyright (C) 2017-2018 Nico Stute <crinq@crinq.de>
* Copyright (C) 2017-2018 Niklas Fauth <niklas.fauth@kit.fail>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "stm32f1xx_hal.h"
#include "defines.h"
#include "setup.h"
#include "config.h"
#include "math.h"
#include "float.h"
//#include "hd44780.h"

#include <ros.h>
#include <std_msgs/String.h> //test
#include <std_msgs/UInt16.h> //test
#include <std_msgs/Int32.h>
#include <std_msgs/Int16.h>
#include <std_msgs/Float32.h>

#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Pose.h>
#include <nav_msgs/Odometry.h>
#include <tf/transform_broadcaster.h>
#include <tf/tf.h>
#include <ros/time.h>
//#include <time.h>
#include <control_msgs/PidState.h>
#include <sensor_msgs/JointState.h>

#define WHEEL_NUM                        2

#define LEFT                             0
#define RIGHT                            1

static void SystemClock_Config(void);


extern DMA_HandleTypeDef hdma_usart2_tx;


UART_HandleTypeDef huart3;
DMA_HandleTypeDef hdma_usart3_rx;
DMA_HandleTypeDef hdma_usart3_tx;

//static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_USART3_UART_Init(void);

int ros_speed_r=0; //test
int ros_speed_l=0; //test

float speed_x = 0.0; //поступательная скорость
float speed_w = 0.0; //вращательная скорость

volatile float v_real = 0.0;
volatile float w_real = 0.0;

float d_r_m=0.000; //
float d_l_m=0.000;
float d_theta=0.000;

// угловые скорости колес
extern float w_l;
extern float w_r;


//одометрия
volatile float x = 0.0;
volatile float y = 0.0;
volatile float theta = 0.0;

//переменные для работы с одометрией
volatile float x_now=0.000;
volatile float y_now=0.000;
volatile float theta_now=0.000;

volatile float x_last=0.000;
volatile float y_last=0.000;
volatile float theta_last=0.000;

volatile float d_x=0.000;
volatile float d_y=0.000;
volatile float d_theta_s=0.000;

volatile float d_s_m=0.000;

//переменные для расчета пройденого растояния
volatile float x_now_s=0.000;
volatile float y_now_s=0.000;

volatile float x_last_s=0.000;
volatile float y_last_s=0.000;

volatile float d_x_s=0.000;
volatile float d_y_s=0.000;


volatile float R=0.000;
volatile float x_r=0.000;
volatile float y_r=0.000;



volatile float Kp_w_l = 2.5;
volatile float Ki_w_l = 5.5;
volatile float Kd_w_l = 0.0;

volatile float error_w_l = 0.0;
volatile float total_error_w_l = 0.0;
volatile float p_error_w_l = 0.0;
volatile float d_error_w_l = 0.0;
volatile float last_error_w_l=0;

volatile float pid_w_l = 0.0;

volatile float Kp_w_r = 2.5;
volatile float Ki_w_r = 5.5;
volatile float Kd_w_r = 0.0;

volatile float error_w_r = 0.0;
volatile float total_error_w_r = 0.0;
volatile float p_error_w_r = 0.0 ;
volatile float d_error_w_r = 0.0;
volatile float last_error_w_r = 0.0;


volatile float pid_w_r = 0.0;

volatile float w_to_l = 0.0; //угловая скорость левого колеса (которое должно быть)
volatile float w_to_r = 0.0; //угловая скорость правого колеса (которое должно быть)



//константы
volatile float L_sepr = 0.497;
volatile float R_wheel = 0.0825;
volatile float pi=3.14159265359;

volatile unsigned long time_cmd_vel_d = 0;
volatile unsigned long time_cmd_vel_now = 0;
volatile unsigned long time_cmd_vel_last = 0;


int cmd_stop_val=1;
int cmd_start_val=0;

//сума тиков с колес
extern volatile int posl_summ;
extern volatile int posr_summ;

//переменные для работы с тиками
volatile int posl_summ_now=0;
volatile int posr_summ_now=0;

volatile int posl_summ_last=0;
volatile int posr_summ_last=0;

volatile int d_posr_summ=0;
volatile int d_posl_summ=0;

volatile int posl_summ_now_w=0;
volatile int posr_summ_now_w=0;

volatile int d_posl_summ_w=0;
volatile int d_posr_summ_w=0;

volatile int posl_summ_last_w=0;
volatile int posr_summ_last_w=0;


volatile int lastSpeedL = 0, lastSpeedR = 0;
volatile int speedL = 0, speedR = 0;
volatile int speedL2 = 0, speedR2 = 0;
volatile float direction = 1;






volatile int chatter_interval = 0;
volatile int chatter_last = 0;


volatile int chatter_interval_odom  = 0;
volatile int chatter_last_odom = 0;


volatile int chatter_last_speed = 0;

volatile int chatter_interval_pid  = 0;
volatile int chatter_last_pid = 0;



void send(void);
void init_nh(void);
void poweroff(void);
ros::Time rosNow(void);


//тестовая функция для проверки взаимодействия колес и топикв ros
void speed_ros( const std_msgs::Int32& cmd_msg){
	ros_speed_r=cmd_msg.data;
	ros_speed_l=cmd_msg.data;
}

//функция обработки топика cmd_vel
void cmd_vel_callback(const geometry_msgs::Twist &vel){
	speed_x = -vel.linear.x;
	speed_w = vel.angular.z;

//	time_cmd_vel_now = HAL_GetTick();
//	time_cmd_vel_d = time_cmd_vel_now - time_cmd_vel_last;
//	time_cmd_vel_last = time_cmd_vel_now;
	time_cmd_vel_last = HAL_GetTick();

	w_to_l=(speed_x+(L_sepr*speed_w)/2.0)/R_wheel;
	w_to_r=(speed_x-(L_sepr*speed_w)/2.0)/R_wheel;

}
void odom_reset_callback(const geometry_msgs::Pose &reset_odom_msg ){
	x = reset_odom_msg.position.x;
	y = reset_odom_msg.position.y;
	theta = reset_odom_msg.orientation.z;
}

void cmd_start_calback(const std_msgs::UInt16& cmd_msg){
	cmd_start_val=cmd_msg.data;
}
void cmd_stop_calback(const std_msgs::UInt16& cmd_msg){
	cmd_stop_val=cmd_msg.data;
}

ros::NodeHandle nh;

//test
std_msgs::String int_msg;
ros::Publisher chatter("chatter", &int_msg);

std_msgs::String str_msg;
ros::Publisher author("author - Ivliev E.A.", &str_msg);


std_msgs::Float32 tikl_msg;
ros::Publisher tikl("tikl", &tikl_msg);

std_msgs::Float32 tikr_msg;
ros::Publisher tikr("tikr", &tikr_msg);


nav_msgs::Odometry odom_msg;
ros::Publisher odom("odom", &odom_msg);

sensor_msgs::JointState joint_states;
ros::Publisher 	joint_states_pub("joint_states", &joint_states);

std_msgs::Float32 battery_level_msg;
ros::Publisher battery_level("battery_level", &battery_level_msg);

std_msgs::Float32 temp_controller_msg;
ros::Publisher temp_controller("temp_controller", &temp_controller_msg);


//тестовый топик
ros::Subscriber<std_msgs::Int32> sub("speed", speed_ros);

//ros::Subscriber<nav_msgs::Odometry>  odom_reset("odom_reset", odom_reset_callback);
ros::Subscriber<geometry_msgs::Pose>  odom_reset("odom_reset", odom_reset_callback);

ros::Subscriber<geometry_msgs::Twist>  cmd_vel("cmd_vel", cmd_vel_callback);
ros::Subscriber<std_msgs::UInt16> cmd_stop("cmd_stop", cmd_stop_calback);
ros::Subscriber<std_msgs::UInt16> cmd_start("cmd_start", cmd_start_calback);


geometry_msgs::TransformStamped odom_tf;
tf::TransformBroadcaster odom_broadcaster;


//pid
control_msgs::PidState pid_wheel_l_val;
ros::Publisher pid_wheel_l("pid_w_l", &pid_wheel_l_val);
control_msgs::PidState pid_wheel_r_val;
ros::Publisher pid_wheel_r("pid_w_r", &pid_wheel_r_val);




extern TIM_HandleTypeDef htim_left;
extern TIM_HandleTypeDef htim_right;
extern ADC_HandleTypeDef hadc1;
extern ADC_HandleTypeDef hadc2;
extern volatile adc_buf_t adc_buffer;

extern UART_HandleTypeDef huart2;




uint8_t button1, button2;

int steer; // global variable for steering. -1000 to 1000
int speed; // global variable for speed. -1000 to 1000

extern volatile int pwml;  // global variable for pwm left. -1000 to 1000
extern volatile int pwmr;  // global variable for pwm right. -1000 to 1000
extern volatile int weakl; // global variable for field weakening left. -1000 to 1000
extern volatile int weakr; // global variable for field weakening right. -1000 to 1000

extern uint8_t buzzerFreq;    // global variable for the buzzer pitch. can be 1, 2, 3, 4, 5, 6, 7...
extern uint8_t buzzerPattern; // global variable for the buzzer pattern. can be 1, 2, 3, 4, 5, 6, 7...

extern uint8_t enable; // global variable for motor enable

//extern volatile uint32_t timeout; // global variable for timeout
uint32_t timeout = 0;
extern float batteryVoltage; // global variable for battery voltage

uint32_t inactivity_timeout_counter;

extern uint8_t nunchuck_data[6];


int milli_vel_error_sum = 0;




float board_temp_adc_filtered = (float)adc_buffer.temp;
float board_temp_deg_c;


uint8_t buf_from_stm_drive[5];

int main(void) {
  HAL_Init();
  __HAL_RCC_AFIO_CLK_ENABLE();
  HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);
  /* System interrupt init*/
  /* MemoryManagement_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(MemoryManagement_IRQn, 0, 0);
  /* BusFault_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(BusFault_IRQn, 0, 0);
  /* UsageFault_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(UsageFault_IRQn, 0, 0);
  /* SVCall_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SVCall_IRQn, 0, 0);
  /* DebugMonitor_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DebugMonitor_IRQn, 0, 0);
  /* PendSV_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(PendSV_IRQn, 0, 0);
  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);

  SystemClock_Config();

  __HAL_RCC_DMA1_CLK_DISABLE();
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_TIM_Init();
  MX_ADC1_Init();
  MX_ADC2_Init();



  HAL_GPIO_WritePin(OFF_PORT, OFF_PIN, GPIO_PIN_SET);

  HAL_ADC_Start(&hadc1);
  HAL_ADC_Start(&hadc2);

  for (int i = 8; i >= 0; i--) {
    // buzzerFreq = i;
    HAL_Delay(100);
  }
//  buzzerFreq = 0;

  HAL_GPIO_WritePin(LED_PORT, LED_PIN, GPIO_PIN_SET);

//  int lastSpeedL = 0, lastSpeedR = 0;
//  int speedL = 0, speedR = 0;
//  int speedL2 = 0, speedR2 = 0;
//  float direction = 1;



// Инициализация Uart
    MX_USART2_UART_Init();
    MX_USART3_UART_Init();




  enable = 1;  // enable motors
  init_nh();




  	  chatter_interval = 1000.0 / 50; //публикация с частотой 50 герц
	  chatter_last = HAL_GetTick();


	  chatter_interval_odom  = 1000.0 / 50;
	  chatter_last_odom = HAL_GetTick();


	  chatter_last_speed = HAL_GetTick();

	  chatter_interval_pid  = 1000.0 / 10;
	  chatter_last_pid = HAL_GetTick();




	  uint8_t test_connekted[3];


  while(1) {
    HAL_Delay(DELAY_IN_MAIN_LOOP); //delay in ms


      timeout = 0;
      // расчет температуры
      board_temp_adc_filtered = board_temp_adc_filtered * 0.99 + (float)adc_buffer.temp * 0.01;
      board_temp_deg_c = ((float)TEMP_CAL_HIGH_DEG_C - (float)TEMP_CAL_LOW_DEG_C) / ((float)TEMP_CAL_HIGH_ADC - (float)TEMP_CAL_LOW_ADC) * (board_temp_adc_filtered - (float)TEMP_CAL_LOW_ADC) + (float)TEMP_CAL_LOW_DEG_C;


//  	if (rosNow().sec > 1559347200){
//  		poweroff();
//  	}


		//////////////////////////////////расчет одометрии
		if(HAL_GetTick() - chatter_last_odom > chatter_interval_odom)
		{
			chatter_last_odom=HAL_GetTick();

			posl_summ_now=posl_summ;
			posr_summ_now=posr_summ;


			d_posl_summ=posl_summ_now-posl_summ_last;
//			d_posl_summ=-1*d_posl_summ;
			d_posr_summ=posr_summ_now-posr_summ_last;
			d_posr_summ=-1*d_posr_summ;

			posl_summ_last=posl_summ_now;
			posr_summ_last=posr_summ_now;



			d_r_m=d_posr_summ*R_wheel*2*pi/90;
			d_l_m=d_posl_summ*R_wheel*2*pi/90;


			if (d_r_m==d_l_m){
				d_theta=(-d_l_m + d_r_m)/L_sepr;
				d_s_m=(d_r_m + d_l_m)/2;


				x+= d_s_m*cos(theta);
				y+= d_s_m*sin(theta);
				theta+=d_theta;
			}
			else{
				R=L_sepr*(d_l_m+d_r_m)/(2*(-d_l_m+d_r_m));
				x_r=x-R*sin(theta);
				y_r=y+R*cos(theta);
				d_theta=(-d_l_m + d_r_m)/L_sepr;

				x=x_r+R*sin(theta+d_theta);
				y=y_r-R*cos(theta+d_theta);
				theta+=d_theta;

			}
		}




		if (cmd_stop_val == 0 && cmd_start_val == 0){
			w_to_l = 0;
			w_to_r = 0;
		}
		else if (cmd_stop_val == 0 && cmd_start_val == 1){
			w_to_l=(speed_x+(L_sepr*speed_w)/2.0)/R_wheel;
			w_to_r=(speed_x-(L_sepr*speed_w)/2.0)/R_wheel;
		}
		else if (cmd_stop_val == 1){
			w_to_l=(speed_x+(L_sepr*speed_w)/2.0)/R_wheel;
			w_to_r=(speed_x-(L_sepr*speed_w)/2.0)/R_wheel;
		}




//		ПЕРЕДАЧА ДАННЫХ ПО UART3 О СОЕДИНЕНИИ С ROS

		if (nh.connected())
		{
			test_connekted[0] = 255;
			test_connekted[1] = 255;
			test_connekted[2] = 1;
			HAL_UART_Transmit_DMA(&huart3, test_connekted, sizeof(test_connekted));
		}

		if (!nh.connected())
		{

			w_to_l = 0;
			w_to_r = 0;
			speed_x = 0;
			speed_w = 0;

			test_connekted[0] = 255;
			test_connekted[1] = 255;
			test_connekted[2] = 0;
			HAL_UART_Transmit_DMA(&huart3, test_connekted, sizeof(test_connekted));
		}


		HAL_UART_Receive_DMA(&huart3,buf_from_stm_drive,5);
		if(buf_from_stm_drive[0] == 255 && buf_from_stm_drive[1] == 255){
			uint8_t Checksum_rx = (~(buf_from_stm_drive[2]+buf_from_stm_drive[3])) & 0xFF;

//			buf_from_stm_drive[2] == 1 for lengs, 2 for arms;
			if(buf_from_stm_drive[2] == 1 && Checksum_rx == buf_from_stm_drive[4]){
				if(buf_from_stm_drive[3] == 1){
					w_to_l = 0;
					w_to_r = 0;
					speed_x = 0;
					speed_w = 0;

//					poweroff(); //test
				}
			}

		}else{
			HAL_UART_Receive_DMA(&huart3,buf_from_stm_drive,1);
		}



		time_cmd_vel_now = HAL_GetTick();
		time_cmd_vel_d = time_cmd_vel_now - time_cmd_vel_last;


		if (time_cmd_vel_d > 4000)
		{

			w_to_l = 0;
			w_to_r = 0;
			speed_x = 0;
			speed_w = 0;

		}



//		if (nh.connected())
//		{
			if(HAL_GetTick() - chatter_last > chatter_interval)
			{
				send();
//				timeout = 0;

			}
//		}






	nh.spinOnce();



	speedL=speedL2;
	speedR=speedR2;

	nh.spinOnce();




    #ifdef ADDITIONAL_CODE
      ADDITIONAL_CODE;
    #endif


    // ####### SET OUTPUTS #######
    if ((speedL < lastSpeedL + 50 && speedL > lastSpeedL - 50) && (speedR < lastSpeedR + 50 && speedR > lastSpeedR - 50) && timeout < TIMEOUT) {
    #ifdef INVERT_R_DIRECTION
//      pwmr = speedR;
    #else
      pwmr = -speedR;
    #endif
    #ifdef INVERT_L_DIRECTION
//      pwml = -speedL;
    #else
      pwml = speedL;
    #endif
    }

    lastSpeedL = speedL;
    lastSpeedR = speedR;


    // ####### POWEROFF BY POWER-BUTTON #######
//    if (HAL_GPIO_ReadPin(BUTTON_PORT, BUTTON_PIN) && weakr == 0 && weakl == 0) {
	if (HAL_GPIO_ReadPin(BUTTON_PORT, BUTTON_PIN) ) {
	      for(int i =0; i<=10; i++){
	      HAL_GPIO_TogglePin(LED_PORT, LED_PIN);
	      HAL_Delay(200);
	      }

		enable = 0;
      while (HAL_GPIO_ReadPin(BUTTON_PORT, BUTTON_PIN)) {}
      poweroff();


	}

    // ####### BEEP AND EMERGENCY POWEROFF #######
    if ((TEMP_POWEROFF_ENABLE && board_temp_deg_c >= TEMP_POWEROFF && abs(speed) < 20) || (batteryVoltage < ((float)BAT_LOW_DEAD * (float)BAT_NUMBER_OF_CELLS) && abs(speed) < 20)) {  // poweroff before mainboard burns OR low bat 3
      poweroff();
    } else if (TEMP_WARNING_ENABLE && board_temp_deg_c >= TEMP_WARNING) {  // beep if mainboard gets hot
      buzzerFreq = 4;
      buzzerPattern = 1;
    } else if (batteryVoltage < ((float)BAT_LOW_LVL1 * (float)BAT_NUMBER_OF_CELLS) && batteryVoltage > ((float)BAT_LOW_LVL2 * (float)BAT_NUMBER_OF_CELLS) && BAT_LOW_LVL1_ENABLE) {  // low bat 1: slow beep
      buzzerFreq = 5;
      buzzerPattern = 42;
    } else if (batteryVoltage < ((float)BAT_LOW_LVL2 * (float)BAT_NUMBER_OF_CELLS) && batteryVoltage > ((float)BAT_LOW_DEAD * (float)BAT_NUMBER_OF_CELLS) && BAT_LOW_LVL2_ENABLE) {  // low bat 2: fast beep
      buzzerFreq = 5;
      buzzerPattern = 6;
    } else if (BEEPS_BACKWARD && speed < -50) {  // backward beep
      buzzerFreq = 5;
      buzzerPattern = 1;
    } else {  // do not beep
//      buzzerFreq = 0;
//      buzzerPattern = 0;
    }


    // ####### INACTIVITY TIMEOUT #######
    if (abs(speedL) >=0 || abs(speedR) >= 0) {
      inactivity_timeout_counter = 0;
    } else {
      inactivity_timeout_counter ++;
    }
    if (inactivity_timeout_counter > (INACTIVITY_TIMEOUT * 60 * 1000) / (DELAY_IN_MAIN_LOOP + 1)) {  // rest of main loop needs maybe 1ms
      poweroff();
    }
  }
}

/** System Clock Configuration
*/
static void SystemClock_Config(void) {
  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

  /**Initializes the CPU, AHB and APB busses clocks
    */
  RCC_OscInitStruct.OscillatorType      = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState            = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState        = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource       = RCC_PLLSOURCE_HSI_DIV2;
  RCC_OscInitStruct.PLL.PLLMUL          = RCC_PLL_MUL16;
  HAL_RCC_OscConfig(&RCC_OscInitStruct);

  /**Initializes the CPU, AHB and APB busses clocks
    */
  RCC_ClkInitStruct.ClockType      = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource   = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider  = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2);

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
  PeriphClkInit.AdcClockSelection    = RCC_ADCPCLK2_DIV8;  // 8 MHz
  HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit);

  /**Configure the Systick interrupt time
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / 1000);

  /**Configure the Systick
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

static void MX_USART2_UART_Init(void)
{

  huart2.Instance = USART2;
  huart2.Init.BaudRate = 57600;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
//    _Error_Handler(__FILE__, __LINE__);
  }

}


/* USART3 init function */
static void MX_USART3_UART_Init(void)
{

  huart3.Instance = USART3;
  huart3.Init.BaudRate = 57600;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
//    _Error_Handler(__FILE__, __LINE__);
  }

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{
  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel6_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel6_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel6_IRQn);
  /* DMA1_Channel7_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel7_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel7_IRQn);


  /* DMA interrupt init */
  /* DMA1_Channel2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel2_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel2_IRQn);
  /* DMA1_Channel3_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel3_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel3_IRQn);

}





void send(void){


			tikl_msg.data =w_l;
			tikl.publish(&tikl_msg);

			tikr_msg.data =w_r;
			tikr.publish(&tikr_msg);

//			chatter_last = HAL_GetTick();

			//публикация напряжения на аккумуляторе
			battery_level_msg.data = batteryVoltage;
			battery_level.publish(&battery_level_msg);
			//публикация температуры контроллера
			temp_controller_msg.data = board_temp_deg_c;
			temp_controller.publish(&temp_controller_msg);
			//публикация odom

			odom_msg.header.frame_id = "odom";
			odom_msg.header.stamp = rosNow();
			odom_msg.child_frame_id = "base_footprint";
			odom_msg.pose.pose.position.x=x;
			odom_msg.pose.pose.position.y=y;
			odom_msg.pose.pose.position.z = 0.0;

			odom_msg.pose.pose.orientation = tf::createQuaternionFromYaw(theta);

			//	пометка тут должна будет реальня скорость
			odom_msg.twist.twist.linear.x = v_real;
			odom_msg.twist.twist.linear.y = 0.0;
			odom_msg.twist.twist.angular.z = w_real;

			odom.publish(&odom_msg);


//			joint_states.header.frame_id = "base_link";
//			joint_states_pub.publish(&joint_states);


			// tf

			odom_tf.header = odom_msg.header;
			odom_tf.header.stamp=rosNow();
			odom_tf.child_frame_id = "base_footprint";
			odom_tf.transform.translation.x = odom_msg.pose.pose.position.x;
			odom_tf.transform.translation.y = odom_msg.pose.pose.position.y;
			odom_tf.transform.translation.z = odom_msg.pose.pose.position.z;
			odom_tf.transform.rotation      = odom_msg.pose.pose.orientation;

//				odom_tf.header.stamp = nh.now();
			odom_broadcaster.sendTransform(odom_tf);



			pid_wheel_l_val.p_error = p_error_w_l;
			pid_wheel_l_val.i_error = error_w_l;
			pid_wheel_l_val.d_error = d_error_w_l;
			pid_wheel_l_val.p_term = Kp_w_l*p_error_w_l;
			pid_wheel_l_val.i_term = Ki_w_l*error_w_l;
			pid_wheel_l_val.d_term = Kd_w_l*d_error_w_l;
			pid_wheel_l_val.output =pid_w_l;

			pid_wheel_l.publish(&pid_wheel_l_val);

			pid_wheel_r_val.p_error = p_error_w_r;
			pid_wheel_r_val.i_error = error_w_r;
			pid_wheel_r_val.d_error = d_error_w_r;
			pid_wheel_r_val.p_term = Kp_w_r*p_error_w_r;
			pid_wheel_r_val.i_term = Ki_w_r*error_w_r;
			pid_wheel_r_val.d_term = Kd_w_r*d_error_w_r;
			pid_wheel_r_val.output =pid_w_r;

			pid_wheel_r.publish(&pid_wheel_r_val);

//			HAL_Delay(5);

}

void init_nh(void){
	//  инициализация узлов и топиков
		nh.initNode();
		nh.advertise(chatter); //test
		nh.advertise(author);
		nh.advertise(battery_level);
		nh.advertise(temp_controller);
		nh.advertise(odom);
		nh.advertise(joint_states_pub);

		nh.advertise(pid_wheel_l);
		nh.advertise(pid_wheel_r);

	//	test
		nh.advertise(tikl);
		nh.advertise(tikr);

		nh.subscribe(sub); //test
		nh.subscribe(odom_reset);
		nh.subscribe(cmd_start);
		nh.subscribe(cmd_stop);
		nh.subscribe(cmd_vel);


		odom_broadcaster.init(nh);

}

void poweroff(void) {
    if (abs(speed) < 20) {
        buzzerPattern = 0;
        enable = 0;
        for (int i = 0; i < 8; i++) {
            // buzzerFreq = i;
            HAL_Delay(100);
        }
        HAL_GPIO_WritePin(OFF_PORT, OFF_PIN, GPIO_PIN_RESET);
        while(1) {}
    }
}


ros::Time rosNow()
{
  return nh.now();
}
